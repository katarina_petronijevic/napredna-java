CREATE DATABASE  IF NOT EXISTS `ideastudio` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ideastudio`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: ideastudio
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `glavniprojektant`
--

DROP TABLE IF EXISTS `glavniprojektant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `glavniprojektant` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ImePrezime` varchar(100) NOT NULL,
  `BrojLicence` bigint NOT NULL,
  `Zvanje` varchar(20) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glavniprojektant`
--

LOCK TABLES `glavniprojektant` WRITE;
/*!40000 ALTER TABLE `glavniprojektant` DISABLE KEYS */;
/*!40000 ALTER TABLE `glavniprojektant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idejnoresenje`
--

DROP TABLE IF EXISTS `idejnoresenje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idejnoresenje` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `DatumIzrade` datetime NOT NULL,
  `GlavniProjektantId` int NOT NULL,
  `ObjekatId` int NOT NULL,
  `LokacijskaDozvolaId` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `idejnoresenje_glavniprojektant_idx` (`GlavniProjektantId`),
  KEY `idejnoresenje_objekat_idx` (`ObjekatId`),
  KEY `idejnoresenje_lokacijskadozvola_idx` (`LokacijskaDozvolaId`),
  CONSTRAINT `idejnoresenje_glavniprojektant` FOREIGN KEY (`GlavniProjektantId`) REFERENCES `glavniprojektant` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idejnoresenje_lokacijskadozvola` FOREIGN KEY (`LokacijskaDozvolaId`) REFERENCES `lokacijskadozvola` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `idejnoresenje_objekat` FOREIGN KEY (`ObjekatId`) REFERENCES `objekat` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idejnoresenje`
--

LOCK TABLES `idejnoresenje` WRITE;
/*!40000 ALTER TABLE `idejnoresenje` DISABLE KEYS */;
/*!40000 ALTER TABLE `idejnoresenje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacijeolokaciji`
--

DROP TABLE IF EXISTS `informacijeolokaciji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `informacijeolokaciji` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `DatumIzdavanja` datetime NOT NULL,
  `NamenaZemljista` varchar(255) NOT NULL,
  `Zona` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacijeolokaciji`
--

LOCK TABLES `informacijeolokaciji` WRITE;
/*!40000 ALTER TABLE `informacijeolokaciji` DISABLE KEYS */;
/*!40000 ALTER TABLE `informacijeolokaciji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lokacijskadozvola`
--

DROP TABLE IF EXISTS `lokacijskadozvola`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lokacijskadozvola` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `OpstiPodaci` varchar(255) NOT NULL,
  `LokacijskiUslovi` varchar(255) NOT NULL,
  `BrojParcele` bigint NOT NULL,
  `PovrsinaParcele` bigint NOT NULL,
  `DatumIzdavanja` datetime NOT NULL,
  `InformacijeOLokacijiId` int NOT NULL,
  `NazivIdejnogResenja` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `lokacijskadozvola_iol_idx` (`InformacijeOLokacijiId`),
  CONSTRAINT `lokacijskadozvola_iol` FOREIGN KEY (`InformacijeOLokacijiId`) REFERENCES `informacijeolokaciji` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lokacijskadozvola`
--

LOCK TABLES `lokacijskadozvola` WRITE;
/*!40000 ALTER TABLE `lokacijskadozvola` DISABLE KEYS */;
/*!40000 ALTER TABLE `lokacijskadozvola` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objekat`
--

DROP TABLE IF EXISTS `objekat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `objekat` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `Dimenzije` decimal(10,0) NOT NULL,
  `Karakteristike` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objekat`
--

LOCK TABLES `objekat` WRITE;
/*!40000 ALTER TABLE `objekat` DISABLE KEYS */;
/*!40000 ALTER TABLE `objekat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `povrsina`
--

DROP TABLE IF EXISTS `povrsina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `povrsina` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Oznaka` int NOT NULL,
  `Naziv` varchar(50) NOT NULL,
  `VrstaPoda` varchar(20) NOT NULL,
  `ProjekatZaGradjevinskuDozvoluId` int NOT NULL,
  `VrstaPovrsineId` int NOT NULL,
  `ProstorijaNaziv` varchar(50) DEFAULT NULL,
  `Status` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `povrsina_pgd_idx` (`ProjekatZaGradjevinskuDozvoluId`),
  KEY `povrsina_vrstapovrsine_idx` (`VrstaPovrsineId`),
  CONSTRAINT `povrsina_pgd` FOREIGN KEY (`ProjekatZaGradjevinskuDozvoluId`) REFERENCES `projekatzagradjevinskudozvolu` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `povrsina_vrstapovrsine` FOREIGN KEY (`VrstaPovrsineId`) REFERENCES `vrstapovrsine` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `povrsina`
--

LOCK TABLES `povrsina` WRITE;
/*!40000 ALTER TABLE `povrsina` DISABLE KEYS */;
/*!40000 ALTER TABLE `povrsina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projekatzagradjevinskudozvolu`
--

DROP TABLE IF EXISTS `projekatzagradjevinskudozvolu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projekatzagradjevinskudozvolu` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `DatumIzrade` datetime NOT NULL,
  `NazivIdejnogResenja` varchar(50) DEFAULT NULL,
  `IdejnoResenjeId` int NOT NULL,
  `StatusDokumenta` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `pgd_idejnoresenje_idx` (`IdejnoResenjeId`),
  CONSTRAINT `pgd_idejnoresenje` FOREIGN KEY (`IdejnoResenjeId`) REFERENCES `idejnoresenje` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projekatzagradjevinskudozvolu`
--

LOCK TABLES `projekatzagradjevinskudozvolu` WRITE;
/*!40000 ALTER TABLE `projekatzagradjevinskudozvolu` DISABLE KEYS */;
/*!40000 ALTER TABLE `projekatzagradjevinskudozvolu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prostorija`
--

DROP TABLE IF EXISTS `prostorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prostorija` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  `VrstaPovrsineId` int NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `prostorija_vrstapovrsine_idx` (`VrstaPovrsineId`),
  CONSTRAINT `prostorija_vrstapovrsine` FOREIGN KEY (`VrstaPovrsineId`) REFERENCES `vrstapovrsine` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prostorija`
--

LOCK TABLES `prostorija` WRITE;
/*!40000 ALTER TABLE `prostorija` DISABLE KEYS */;
/*!40000 ALTER TABLE `prostorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrstapovrsine`
--

DROP TABLE IF EXISTS `vrstapovrsine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vrstapovrsine` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Naziv` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrstapovrsine`
--

LOCK TABLES `vrstapovrsine` WRITE;
/*!40000 ALTER TABLE `vrstapovrsine` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrstapovrsine` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-17 17:05:53
